import Foundation

extension String {
    subscript(idx: Int) -> Character {
        guard let strIdx = index(startIndex, offsetBy: idx, limitedBy: endIndex) else {
            fatalError("String index out of bounds") }
        return self[strIdx]
    }
}
