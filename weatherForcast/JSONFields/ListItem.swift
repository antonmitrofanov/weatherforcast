import Foundation

class ListItem: Codable {
    
    var main: Main?
    var weather: [Weather]?
    var wind: Wind?
    var dt_txt: String?
    
}
