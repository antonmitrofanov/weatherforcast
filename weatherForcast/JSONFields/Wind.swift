import Foundation

class Wind: Codable {
    
    var speed: Double?
    var deg: Double?
    
}
