import Foundation

class Manager {
    
    static let shared = Manager()
    private init(){}
    
    var fiveDaysForcast: [ListItem] = []
    var fiveDaysForcastString: [String] = []
    
    func getJSON(city: String?, latitude: String?, longitude: String?, completion: @escaping (_ result: JSONResponse?, _ error: Error?) -> ()) {
        var myUrl: URL?
        if let city = city {
            guard let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&APPID=b96ecfa81d2cb687775b7a54a06ca94a") else {
                return
            }
            myUrl = url
        } else {
            guard let latitude = latitude, let longitude = longitude else {
                return
            }
            guard let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?lat=" + latitude + "&lon=" + longitude + "&APPID=b96ecfa81d2cb687775b7a54a06ca94a") else {
                return
            }
            myUrl = url
        }
        
        if let url = myUrl {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request) { (data, request, error) in
                if error == nil, let data = data {
                    if let response = try? JSONDecoder().decode(JSONResponse.self, from: data) {
                        completion(response, nil)
                    } else {
                        print()
                    }
                } else {
                    let error = NSError(domain: "ERROR", code: 0, userInfo: [:])
                    print(error)
                    completion(nil, error)
                }
            }
            task.resume()
        }
    }
    
}
