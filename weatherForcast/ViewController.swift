import UIKit
import Foundation
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var directionLabel: UILabel!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var findButton: UIButton!
    
    let navBarMinHeight: CGFloat = 90
    let navBarMaxHeight: CGFloat = 180
    let searchButtonImage = UIImage(named: "search")
    let cancelImage = UIImage(named: "cancel")
    let animationDuration = 0.3
    let cornerRadius: CGFloat = 10
    var searchBarIsHidden = true
    let date = Date()
    let formatter = DateFormatter()
    var locationManager = CLLocationManager()
    var city: String?
    var latitude: String?
    var longitude: String?
    
    override func viewWillAppear(_ animated: Bool) {
        self.searchView.isHidden = true
        self.findButton.layer.cornerRadius = self.cornerRadius
        self.formatter.dateFormat = "yyyy-MM-dd"
        self.todayLabel.text = "Today is " + formatter.string(from: self.date)
    }
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        self.searchBarIsHidden = !self.searchBarIsHidden
        if self.searchBarIsHidden {
            self.searchView.isHidden = true
            self.tableView.reloadData()
            self.navBarHeightConstraint.constant = self.navBarMinHeight
            UIView.animate(withDuration: self.animationDuration) {
                self.view.layoutIfNeeded()
            }
            sender.setImage(self.searchButtonImage, for: UIControl.State.normal)
        } else {
            self.navBarHeightConstraint.constant = self.navBarMaxHeight
            self.tableView.reloadData()
            UIView.animate(withDuration: self.animationDuration, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.searchView.isHidden = false
            }
            sender.setImage(self.cancelImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func findButtonPressed(_ sender: UIButton) {
        self.latitude = nil
        self.longitude = nil
        self.city = nil
        Manager.shared.fiveDaysForcast.removeAll()
        Manager.shared.fiveDaysForcastString.removeAll()
        if let city = self.searchTextField.text {
            self.city = city
            self.getWeather(city: city, longitude: nil, latitude: nil)
        }
    }
    
    @IBAction func locationButtonPressed(_ sender: UIButton) {
        self.city = nil
        self.latitude = nil
        self.longitude = nil
        self.searchTextField.text = ""
        Manager.shared.fiveDaysForcast.removeAll()
        Manager.shared.fiveDaysForcastString.removeAll()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    @IBAction func refreshButtonPressed(_ sender: UIButton) {
        Manager.shared.fiveDaysForcast.removeAll()
        Manager.shared.fiveDaysForcastString.removeAll()
        if let city = self.city {
            self.getWeather(city: city, longitude: nil, latitude: nil)
        } else {
            guard let latitude =  self.latitude, let longitude = self.longitude else {
                return
            }
            getWeather(city: nil, longitude: longitude, latitude: latitude)
        }
    }
    
    func getWeather(city: String?, longitude: String?, latitude: String?) {
        Manager.shared.getJSON(city: city, latitude: latitude, longitude: longitude) { [weak self] result, error in
            if let error = error {
                print(error)
            } else {
                guard let list = result?.list else {
                    return
                }
                DispatchQueue.main.async {
                    if let city = city {
                        self?.cityLabel.text = city
                    } else {
                        guard let latitude = latitude, let longitude = longitude else {
                            return
                        }
                        self?.cityLabel.text = "Your location"
                    }
                    
                    
                    if let firstItem = list[0] {
                        if let main = firstItem.main {
                            if let temp = main.temp {
                                self?.tempLabel.text = String(round(temp) - 273) + " °C"
                            } else {
                                self?.tempLabel.text = "-"
                            }
                        }
                        if let wind = firstItem.wind {
                            if let speed = wind.speed {
                                self?.windLabel.text = String(speed) + " m/s"
                            } else {
                                self?.windLabel.text = "-"
                            }
                            if let deg = wind.deg {
                                self?.directionLabel.text = String(deg) + " °"
                            } else {
                                self?.directionLabel.text = "-"
                            }
                        }
                        if let weather = firstItem.weather {
                            if let desc = weather[0].description {
                                self?.precipitationLabel.text = desc
                            } else {
                                self?.precipitationLabel.text = "-"
                            }
                        }
                        if let now = firstItem.dt_txt {
                            let currDay = String(now[9]) + String(now[10])
                            for listItem in list {
                                if let listItem = listItem {
                                    if let then = listItem.dt_txt {
                                        let nextDay = String(then[9]) + String(then[10])
                                        if nextDay != currDay && !Manager.shared.fiveDaysForcastString.contains(nextDay) {
                                            Manager.shared.fiveDaysForcast.append(listItem)
                                            Manager.shared.fiveDaysForcastString.append(nextDay)
                                        }
                                    }
                                }
                            }
                        }
                        self?.tableView.reloadData()
                    }
                }
            }
        }
        
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Manager.shared.fiveDaysForcast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        cell.setupCell(item: Manager.shared.fiveDaysForcast[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.height/CGFloat(Manager.shared.fiveDaysForcast.count)
    }
    
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation = locations.first as? CLLocation {
            let coordinates = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
            let latitude = String(coordinates.latitude)
            let longitude = String(coordinates.longitude)
            print(latitude)
            self.latitude = latitude
            self.longitude = longitude
            self.getWeather(city: nil, longitude: longitude, latitude: latitude)
            self.locationManager.stopUpdatingLocation()
        }
    }
    
}

