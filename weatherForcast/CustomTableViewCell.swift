

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    func setupCell(item: ListItem) {
        if let main = item.main {
            if let temp = main.temp {
                self.tempLabel.text = String(round(temp-273)) + " °"
            } else {
                self.tempLabel.text = ""
            }
        }
        if let weather = item.weather {
            if let description = weather[0].description {
                self.weatherLabel.text = description
            } else {
                self.weatherLabel.text = ""
            }
        }
        
        if let dt_txt = item.dt_txt {
            var date = ""
            for n in 0...10 {
                date = date + String(dt_txt[n])
            }
            self.dateLabel.text = date 
        }
    }

}
